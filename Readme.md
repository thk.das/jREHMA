# Java-based REST-ful HTTP Message Authentication (jREHMA)
This is the jREHMA project, a Java library for signing and verifying REST-ful HTTP Messages.
jREHMA is an implementation of the REST-ful Message Authentication (REMA) scheme based on the paper "Authentication Scheme for REST" [1].

## Version
0.1 Pre-Alpha

## Download
* JAR containing the binaries: http://das.th-koeln.de/REMA/REHMA/jREHMA/jREHMA-v.0.1-pre-alpha.jar
* JAR containing the sources:  http://das.th-koeln.de/REMA/REHMA/jREHMA/jREHMA-v.0.1-pre-alpha-sources.jar
* JAR containing only test classes: http://das.th-koeln.de/REMA/REHMA/jREHMA/jREHMA-v.0.1-pre-alpha-test-classes.jar
* JAR containing only test classes with sources: http://das.th-koeln.de/REMA/REHMA/jREHMA/jREHMA-v.0.1-pre-alpha-test-classes-sources.jar

## Dependencies
* [Apache HttpComponents 4.5.1](https://hc.apache.org/downloads.cgi)
* [Apache Commons IO 2.4](https://commons.apache.org/proper/commons-io/download_io.cgi)
* [Apache Commons Lang 3.4](https://commons.apache.org/proper/commons-lang/download_lang.cgi)
* [Junit 4.12](https://github.com/junit-team/junit/wiki/Download-and-Install)

# Requirements
Java 1.7 or higher

## Getting Started
Please visit the Wiki for further information on working with jREHMA:

[Wiki](https://gitlab.com/thk.das/jREHMA/wikis/home)

[JavaDocs](http://das.th-koeln.de/REMA/REHMA/jREHMA/doc/)

# References
[1] Luigi Lo Iacono and Hoai Viet Nguyen: "Authentication Scheme for REST", in International Conference on Future Network Systems and Security (FNSS), 2015.
[Online]. Available at: [http://link.springer.com/chapter/10.1007/978-3-319-19210-9_8](http://link.springer.com/chapter/10.1007/978-3-319-19210-9_8)