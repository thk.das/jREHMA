/*******************************************************************************
 * Copyright 2015 Hoai Viet Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.thk.das.rest.security.http.rehma;
import java.util.List;

import org.apache.http.HttpResponse;

import de.thk.das.rest.security.http.rehma.hash.BodyHasher;
import de.thk.das.rest.security.http.rehma.sig.TbsAuthenticator;
import de.thk.das.rest.security.http.rehma.utils.SignatureHeaderParser;

public class ResponseVerifyer extends ResponseAuthentication {
	
	public ResponseVerifyer(){
		super();
	}
	
	public ResponseVerifyer(TbsAuthenticator tbsAuthenticator, BodyHasher bodyHasher) {
		this();
		addTbsAuthenticator(tbsAuthenticator);
		addBodyHasher(bodyHasher);
	}
	
	protected ResponseVerifyer(List<TbsAuthenticator> tbsAuthenticators, List<BodyHasher> bodyHashers){
		this();
		addTbsAuthenticators(tbsAuthenticators);
		addBodyHashers(bodyHashers);
	}
	
	public void verify(HttpResponse res, String requestMethod) throws Exception{
		
		if(res.containsHeader("Signature")){
			SignatureHeaderParser parser = new SignatureHeaderParser(res.getFirstHeader("Signature").getValue());
			parser.parse();
			String tvp = parser.getTvp();
			if(!getTimeVariantParameter().verify(tvp)){
				throw new NotAuthenticatedExpection("Invalid TVP");
			}
			
			String kid = parser.getKid();
			String sv = parser.getSigValue();
			String hash = parser.getHash();
			String sig = parser.getSig();
			List<String> additionalHeaders = parser.getAddHeaders();
			
			String tbs = buildTbs(res, requestMethod,hash,tvp,additionalHeaders);
			
			TbsAuthenticator tbsAuthenticator = getTbsAuthenticatorsHashMap().get(sig);
			
			if(tbsAuthenticator == null){
				throw new UnsupportedSignatureAlgorithm();
			}
			
			if(!tbsAuthenticator.verify(kid, tbs, sv)){
				throw new NotAuthenticatedExpection("Signature invalid");
			}
			
		} else {
			throw new NotAuthenticatedExpection("No Signature Header");
		}
	}

}
