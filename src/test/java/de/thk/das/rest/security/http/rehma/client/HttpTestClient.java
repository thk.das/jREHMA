/*******************************************************************************
 * Copyright 2015 Hoai Viet Nguyen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.thk.das.rest.security.http.rehma.client;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpCoreContext;

import de.thk.das.rest.security.http.rehma.ahc.SigningRequestInterceptor;
import de.thk.das.rest.security.http.rehma.ahc.VerifyingResponseInterceptor;
import de.thk.das.rest.security.http.rehma.hash.Sha256Hasher;
import de.thk.das.rest.security.http.rehma.sig.HmacSha256Authenticator;
/**
 * jREHMA HTTP test client
 */
public class HttpTestClient {
	public static void main(String[] args) throws ClientProtocolException, IOException {
		
		String base64Key = "";
		BufferedReader br = new BufferedReader(new FileReader("key.txt"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        base64Key = sb.toString();
	    } finally {
	        br.close();
	    }
	    byte[] key = Base64.decodeBase64(base64Key); 
	    
	    Sha256Hasher  sha256Hasher = new Sha256Hasher();
	    HmacSha256Authenticator hmacSha256Authenticator = new HmacSha256Authenticator();
	    hmacSha256Authenticator.getHmacKeyStore().put("jREHMAKey", key);
	    
	    SigningRequestInterceptor sri = new SigningRequestInterceptor("jREHMAKey",hmacSha256Authenticator, sha256Hasher);
	    VerifyingResponseInterceptor vri = new VerifyingResponseInterceptor(hmacSha256Authenticator, sha256Hasher);
		
	    HttpClient client = HttpClients.custom().addInterceptorLast(sri).addInterceptorFirst(vri).build();
		
		HttpPut putRequest = new HttpPut("http://localhost:8088/courses");
		putRequest.setHeader("Content-Type", "application/json");
		putRequest.setHeader("User-Agent", "jREHMA-Client");
		putRequest.setEntity(new StringEntity("{}"));
		
		HttpPost postRequest = new HttpPost("http://localhost:8088/courses");
		postRequest.setHeader("Content-Type", "application/json");
		postRequest.setHeader("User-Agent", "jREHMA-Client");
		postRequest.setEntity(new StringEntity("{}"));
		
		HttpGet getRequest = new HttpGet("http://localhost:8088/courses");
		getRequest.setHeader("Accept", "application/json");
		
		HttpCoreContext ctx = new HttpCoreContext();
		
//		HttpResponse response = client.execute(putRequest,ctx);

//		HttpResponse response = client.execute(postRequest,ctx);

		HttpResponse response = client.execute(getRequest,ctx);
		
		System.out.println(ctx.getRequest());
		System.out.println(response);
	}
}
